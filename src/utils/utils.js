/* 深拷贝 */
/**
 * 深拷贝函数
 * @param {*} payload 要拷贝的对象或值
 * @param {Map} map 用于记录已经拷贝过的对象的映射
 * @returns {*} 返回深拷贝后的对象或值
 */
export const deepClone = (payload, map = new Map()) => {
  if (map.has(payload)) return map.get(payload);
  if (typeof payload === "function") return eval(payload.toString());
  if (typeof payload !== "object") return payload;
  let res;
  const constructor = payload.constructor;
  switch (constructor) {
    case String:
    case Number:
    case Boolean:
    case Date:
    case RegExp:
      return new constructor(payload);
    default:
      res = new constructor();
      map.set(payload, res);
  }
  Reflect.ownKeys(payload).forEach((key) => {
    res[key] = deepClone(payload[key], map);
  });
  return res;
};

/* 防抖 */
/**
 * 防抖函数
 * @param {*} fn 要执行的函数
 * @param {number} wait 延迟时间
 * @returns {Function} 返回一个新函数
 */
export const debounce = (fn, wait) => {
  let timer = null;
  return () => {
    if (timer !== null) {
      clearTimeout(timer);
    }
    timer = setTimeout(fn, wait);
  };
};

/* 处理图片数组转string */
/**
 * 将图片数组转换为逗号分隔的字符串
 * @param {Array} arr 图片数组
 * @param {string} key 图片属性键名
 * @returns {string} 返回转换后的字符串
 */
export const arrayImgsToString = (arr, key) => arr.map(item => item[key]).join(",");

/* 判断数组相同 */
/**
 * 判断两个数组是否相同
 * @param {Array} arr1 第一个数组
 * @param {Array} arr2 第二个数组
 * @returns {boolean} 返回是否相同的布尔值
 */
export const arraysContentAreEqual = (arr1, arr2) => {
  if (arr1.length !== arr2.length) {
    return false;
  }
  const sortedArr1 = arr1.slice().sort();
  const sortedArr2 = arr2.slice().sort();
  return sortedArr1.every((value, index) => value === sortedArr2[index]);
};

/* 判断数据类型 */
/**
 * 返回指定数据类型的默认值
 * @param {string} payload 数据类型字符串
 * @returns {*} 返回对应数据类型的默认值
 */
export const dataDefault = (payload) => ({
  "Array": [],
  "Object": {},
}[payload]);

/**
 * 
 * @param {*} i 
 * @returns 
 */
export const getCharCode = (i) => {
  return String.fromCharCode(97 + i).toUpperCase();
};
