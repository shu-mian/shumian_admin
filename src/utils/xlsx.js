import { utils, write, writeFile } from "xlsx";
import { findAll } from "@/service/base.service";
import moment from "moment";



/**
 * 
 */
export class Workbook {
    constructor() {
        this.wb = utils.book_new();
    }

    addSheet(sheet) {
        utils.book_append_sheet(this.wb, sheet.ws, sheet.name);
    }

    setValidation(sheet, dataValidationRule) {
        sheet.ws['!dataValidation'] = [dataValidationRule];
    }

    writeToBuffer() {
        return write(this.wb, { type: 'buffer' });
    }

    writeToFile(fileName) {
        writeFile(this.wb, fileName);
    }
}

/**
 * 
 * @param {string} name 表格名称
 * @param {array} columns 表头
 * @param {array} columnData 表数据 
 * 
 * */
export class Sheet {
    constructor({ name, columns, columnData }) {
        this.name = name;
        this.columns = columns;
        this.columnData = columnData;
        this.sheetData = [this.columns, ...this.columnData];
        this.ws = utils.aoa_to_sheet(this.sheetData);
        this.setColWidth();
    }

    // 设置列宽
    setColWidth() {
        const wscols = this.columns.map(() => ({ wch: 30 }));

        // 遍历数据并调整列宽
        this.sheetData.forEach(row => {
            row.forEach((cell, colIndex) => {
                const cellLength = String(cell).length;
                if (!wscols[colIndex] || cellLength > wscols[colIndex].wch) {
                    wscols[colIndex] = { wch: cellLength };
                }
            });
        });

        // 将列宽应用到工作表
        this.ws['!cols'] = wscols;
    }
}

/**
 * generateData 生成excel数据
 * @param {array} list 需要导出的数据列表
 * @param {array} tableColumns 表格表头
 * @param {object} fields 字段对象
 *
 */
export const generateData = (list, tableColumns, fields) => {
    const filterColumns = tableColumns.filter(column => Object.keys(fields).some(key => column.key == key))
    const sheetColumns = filterColumns.map((column) => column.title);
    const sheetColumnData = typeof list == 'number' ? new Array(list).fill(sheetColumns.map(item => "")) : list.map((item) =>
        filterColumns.map((column) => data_dict(column, item, fields))
    );
    return { columns: sheetColumns, columnData: [...sheetColumnData] };
};

/**
 * 
 * @param {array} columns 
 * @param {object} fields 
 * @returns 
 */
export const generateXlsxDict = (columns, fields) => {
    return { columns: ['cn', 'en', 'type'], columnData: [...Object.keys(fields).map(field => [columns.filter(column => column.key == field)[0]['title'], field, fields[field].type])] };
};

/**
 * 字段翻译
 * @param {object} column 列
 * @param {object} record 行
 * @param {object} fields 字段集合
 * @returns {string}
 */
export const data_dict = (column, record, fields) => {
    const TYPE = fields[column.key].type;
    const isPointerArray = TYPE === 'Array' && fields[column.key]?.isPointer;

    // 处理不同类型的字段
    switch (TYPE) {
        case 'Object':
            return JSON.stringify(record[column.key], null, 2) || "";
        case 'Array':
            if (isPointerArray) {
                return (record[column.key]?.map(item => item.name || item.objectId) || []).join(",");
            }
            return record[column.key].join(',') || "";
        case 'Date':
            return moment(record[column.key]).format("YYYY-MM-DD HH:mm:ss");
        case 'Pointer':
            return record[column.key]?.name || record[column.key];
        case 'Boolean':
            return record[column.key] ? '是' : '否';
        default:
            return record[column.key] || "";
    }

}


/**
 * xlsx模版中的选择列表
 * @param {*} columnOption 
 * @returns 
 */
export const xlsx_select = async (columnOption) => {
    const TYPE = columnOption.type
    switch (TYPE) {
        case 'Pointer':
            return (await findAll(columnOption.targetClass));
        case 'Array':
            if (columnOption.targetClass) {
                return (await findAll(columnOption.targetClass));
            }
            return false
        case 'String':
            if (columnOption.componentOption.selectTable) {
                return (await findAll(columnOption.componentOption.selectTable));
            }
            return false
        default:
            return false;
    }
}

/**
 * 获取列数据
 * @param {*} list 
 * @returns {array}
 */
export const get_xlsx_columns = (list) => {
    return list.filter((item, index) => index)
        .map((info) => {
            return new Object({
                title: info[0],
                dataIndex: info[1],
                key: info[1],
            });
        })
}

/**
 * 获取表格数据
 * @param {*} list 
 * @param {*} columns 
 * @returns {array}
 */
export const get_xlsx_data = (list, columns) => {
    return list.filter((item, idx) => idx).map((item) => {
        const obj = {}
        columns.forEach((col, index) => {
            obj[col.key] = item[index]
        })
        return obj
    })
}
