/**
 * 
 * @param {*} file 
 * @returns 
 */
export const readFile = (file) => {
    return new Promise((resolve, reject) => {
        try {
            const reader = new FileReader();
            reader.onload = (e) => {
                resolve(new Uint8Array(e.target.result));
            };
            reader.readAsArrayBuffer(file);
        } catch (error) {
            reject(error)
        }

    })
}