import Parse from "parse";

/* 节点是否已选 */
/**
 * 
 * @param {*} className 
 * @param {*} classField 
 * @param {*} selectId 
 * @returns 
 */
export const isSelected = async (className, classField, selectId) => {
  const table = new Parse.Query(className);
  table.containedIn(classField, [selectId]);
  return await table.count();
};
