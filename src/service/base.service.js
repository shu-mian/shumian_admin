import Parse from "parse";
import { Capture, FindList, handleParseError } from "@/service/service.config";
import { notification } from "ant-design-vue";

/* 搜索数据的方法 */
const machMethod = (field) => {
  // 定义字段类型和相应的方法映射
  const methodMap = {
    Pointer: "equalTo",
    Array: "containedIn",
    String: "contains",
    Boolean: "equalTo",
  };

  // 检查字段类型是否在映射中
  if (field.type in methodMap) {
    return methodMap[field.type];
  } else {
    // 如果字段类型不匹配，返回一个默认值或抛出错误
    console.error(`未知的字段类型：${field.type}`);
    return null;
  }
};

/* 查询所有数据 */
export const findAll = async (
  className,
  where = { company: sessionStorage.getItem("companyId") }
) => {
  const table = new Parse.Query(className == "_User" ? Parse.User : className);
  Object.keys(where).forEach((key) => {
    if (!["AntdIcon", "UserLogs", "_Session", "Permission"].includes(className) && where[key]) {
      table.equalTo(key, where[key]);
    }
  });
  table.limit(10000);
  table.descending("createdAt");
  table.includeAll();
  return (
    await table.find().catch((err) => {
      handleParseError(err);
    })
  )
    ?.filter((item) => {
      return item.id;
    })
    ?.map((item) => {
      return item.toJSON();
    });
};

/* 查询列表 */
export const findList = async (query, where = {}) => {
  const { className, pageSize, pageNum } = query;
  where = Object.assign({}, where, query.where);
  if (!className) return;
  const table = new Parse.Query(className == "_User" ? Parse.User : className);
  if (!["AntdIcon", "UserLogs", "_Session"].includes(className)) {
    table.equalTo("company", sessionStorage.getItem("companyId"));
  }
  Object.keys(where).forEach((key) => {
    if (where[key]) table[machMethod(query.filterFilds[key])](key, where[key]);
  });
  table.limit(pageSize || 200);
  table.skip((pageSize || 200) * (pageNum ? pageNum - 1 : 0));
  table.descending("createdAt");
  table.includeAll();
  return new FindList(
    (
      await table.find().catch((err) => {
        handleParseError(err);
      })
    )?.map((item) => item.toJSON()),
    await table.count().catch((err) => {
      handleParseError(err);
    })
  );
};

/* 查询字段 */
export const findSchema = async (query) => {
  const table = new Parse.Query("Schema");
  table.equalTo("name", query.className);
  table.includeAll();
  return await table.first().catch((err) => {
    handleParseError(err);
  });
};

// 提取单条记录处理逻辑
const prepareTable = (className, fields, params) => {
  const Table = Parse.Object.extend(className);
  const table = new Table();

  Object.keys(params).forEach((key) => {
    const TYPE = fields[key].type;
    if (TYPE === "Pointer" && params[key]) {
      table.set(key, {
        __type: TYPE,
        className: fields[key].targetClass,
        objectId: params[key],
      });
    } else if (TYPE === "Array" && fields[key].isPointer) {
      params[key] = typeof params[key] === 'string' ? params[key].split(',') : params[key];
      table.set(
        key,
        params[key]?.map((item) => ({
          __type: "Pointer",
          className: fields[key].targetClass,
          objectId: item,
        }))
      );
    } else if (TYPE === "Number") {
      table.set(key, Number(params[key]));
    } else if (TYPE === "String") {
      table.set(key, String(params[key]));
    } else {
      table.set(key, params[key]);
    }
  });

  if (sessionStorage.getItem("companyId")) {
    table.set("company", {
      __type: "Pointer",
      className: "Company",
      objectId: sessionStorage.getItem("companyId"),
    });
  }

  const acl = new Parse.ACL();
  acl.setPublicReadAccess(true);
  acl.setPublicWriteAccess(true);
  table.setACL(acl);

  return table;
};

// 支持单条和批量插入
export const InsertRow = async ({ className, fields, params }) => {
  const isArray = Array.isArray(params);
  const tables = isArray
    ? params.map((param) => prepareTable(className, fields, param))
    : [prepareTable(className, fields, params)];

  try {
    const results = await Parse.Object.saveAll(tables);

    results.forEach((result) => {
      notification.success({
        message: sessionStorage.getItem("pageName"),
        description: (result.get("name") || result.get("objectId")) + "新增成功",
        duration: 0.5,
      });
      // UpdateTablePermission(className, result);
    });

    return results;
  } catch (error) {
    console.error("Error saving objects:", error);
    throw error;
  }
};

// 提取单条记录更新逻辑
const updateTableFields = (table, fields, params) => {
  Object.keys(params).forEach((key) => {
    if (key !== "objectId") {
      const TYPE = fields[key].type;
      if (TYPE === "Pointer" && params[key]) {
        table.set(key, {
          __type: TYPE,
          className: fields[key].targetClass,
          objectId: params[key],
        });
      } else if (TYPE === "Array" && fields[key].isPointer) {
        params[key] = typeof params[key] === 'string' ? params[key].split(',') : params[key];
        table.set(
          key,
          params[key]?.map((item) => ({
            __type: "Pointer",
            className: fields[key].targetClass,
            objectId: item,
          }))
        );
      } else {
        table.set(key, params[key]);
      }
    }
  });

  if (sessionStorage.getItem("companyId")) {
    table.set("company", {
      __type: "Pointer",
      className: "Company",
      objectId: sessionStorage.getItem("companyId"),
    });
  }
};

/**
 * 
 * @param {*} param0 
 * @returns 
 */
export const UpdateById = async ({ className, fields, params }) => {
  try {
    const Table = new Parse.Query(className);
    Table.equalTo("objectId", params.objectId);
    const table = await Table.first();

    if (table) {
      updateTableFields(table, fields, params);

      const success = await table.save();
      notification.success({
        message: sessionStorage.getItem("pageName"),
        description: `${success.get("name") || success.get("objectId")}修改成功`,
        duration: 0.5,
      });
      return success;
    } else {
      throw new Error(`Object with id ${params.objectId} not found`);
    }
  } catch (error) {
    console.error("Error updating object:", error);
    throw error;
  }
};


/* 删除一行 */
export const removeById = async ({ className, objectId }) => {
  const Table = new Parse.Query(className);
  Table.equalTo("objectId", objectId);
  const table = await Table.first();
  return await Capture(table.destroy()).then((success) => {
    notification.success({
      message: sessionStorage.getItem("pageName"),
      description:
        (success.get("name") || success.get("objectId")) + "删除成功",
      duration: 0.5,
    });
  });
};
