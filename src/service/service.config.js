
import { notification } from "ant-design-vue";
import Parse from "parse";

// 定义具体的错误处理函数
const handleInvalidSessionToken = () => {
    localStorage.clear();
    location.reload();
};

const handleDefaultError = (err) => {
    notification.error({
        message: `Error Code: ${err.code}`,
        description: err.message || String(err),
        duration: 0.5,
    });
};

// 主错误处理函数
export const handleParseError = (err) => {
    switch (err.code) {
        case Parse.Error.INVALID_SESSION_TOKEN:
            handleInvalidSessionToken();
            break;
        // 这里可以添加更多的错误代码处理
        case Parse.Error.OBJECT_NOT_FOUND:
            notification.error({
                message: 'Object not found',
                description: 'The specified object was not found in the database.',
                duration: 0.5,
            });
            break;
        case Parse.Error.CONNECTION_FAILED:
            notification.error({
                message: 'Connection failed',
                description: 'Failed to connect to the server. Please check your internet connection.',
                duration: 0.5,
            });
            break;
        case Parse.Error.DUPLICATE_VALUE:
            notification.error({
                message: 'Duplicate value',
                description: 'A duplicate value was found. Please ensure the values are unique.',
                duration: 0.5,
            });
            break;
        // 添加其他错误处理逻辑
        default:
            handleDefaultError(err);
            break;
    }
};

export const Capture = async (func) => {
    try {
        const success = await func;
        return success;
    } catch (error) {
        notification.error({
            message: sessionStorage.getItem('pageName') || 'Error',
            description: error.message || error.toString(),
            duration: 0.5,
        });
        handleParseError(error);
        throw error; // 继续抛出错误，以便调用方可以处理
    }
};

export class FindList {
    constructor(data, count) {
        this.data = data
        this.count = count
    }

    setData(arg) {
        this.data = arg
    }

    setCount(arg) {
        this.count = arg
    }

    getData() {
        return this.data
    }

    getCount() {
        return this.count = arg
    }
}